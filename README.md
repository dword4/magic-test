# hockey-info
An app written in Python utilizing Flask and the NHL API to present hockey data
in a simple and easy to view manner with a focus on mobile users.

# Disclaimer
I do this for fun, there is absolutely no warranty or guarantee with any of this.
I don't have rights to anything the NHL does, just another hockey nerd that knows
a few neat programming tricks trying to help others.

# Want to see it first?

[Hockey-Info.online](https://hockey-info.online) is exactly the code you see here plus the TRACKING_CODE environment variable so I can have some stats to obsess over.

# How to run it?
```
export FLASK_APP=app.py
export TRACKING_CODE=''
flask run
```
It will start a server running on localhost and accessible on port 5000 (http://127.0.0.1:5000/)


If you need to run it on a different port invoke flask thusly

```
flask run --host=0.0.0.0
```

If you happen to have a Docker server there is a Dockerfile included or you can pull directly from 
the container registery [container registery](registry.gitlab.com/dword4/hockey-info)

# Contact

[@dw0rd4](https://twitter.com/dw0rd4) on Twitter
